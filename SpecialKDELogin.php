<?php

if ( !defined( 'MEDIAWIKI' ) ) {
    die( 'This is a MediaWiki extension, and must be run from within MediaWiki.' );
}

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/includes/KDEProvider.php';

class SpecialKDELogin extends SpecialPage
{
    // save an instance of the oauth2 login
    private $client;

    // The oauth data about the selected user
    private $resourceOwner;

    private $extuser;

    // Allow only logins with KDE
    private $kdeOnlyLogin = false;

    public function __construct() {
        parent::__construct( 'KDELogin', 'kdelogin' );
        $this->listed = true;

        global $wgKDELogin, $wgServer, $wgArticlePath;

        if( !isset($wgKDELogin['kdeonly']) || $wgKDELogin['kdeonly'] === null ) {
            $this->kdeOnlyLogin = $wgKDELogin['kdeonly'];
        }

        $this->client = new Mediawiki\Extensions\KDELogin\KDEProvider(
            $wgKDELogin['baseurl'],
            [
                'clientId'      => $wgKDELogin['clientid'],
                'clientSecret'  => $wgKDELogin['clientsecret'],
                'redirectUri'   => $wgServer . str_replace( '$1', 'Special:KDELogin/callback', $wgArticlePath ),
            ]
        );

    }

    public function execute($parameter) {
        $user = $this->getUser();
        $out = $this->getOutput();
        $request = $this->getRequest();
        $this->setHeaders();

        // persist session
        $request->getSession()->persist();

        switch($parameter){
            case 'redirect':
                $this->_redirect();
                break;
            case 'callback':
                $this->_handleCallback();
                break;
            case 'logout':
                $this->_logout();
                break;
            case 'finish':
                $this->_finish();
                break;
            default:
                $this->_default();
            break;
        }
    }

    /**
     * Redirect to the MyKDE Instance
     */
    private function _redirect() {

        $request = $this->getRequest();
        $session = $request->getSession();

        if (!isset($_GET['code'])) {

            // Fetch the authorization URL from the provider; this returns the
            // urlAuthorize option and generates and applies any necessary parameters
            // (e.g. state).
            $options = [
                'scopes' => ['email']
            ];
            $authorizationUrl = $this->client->getAuthorizationUrl($options);

            // Get the state generated for you and store it to the session.
            $session->set('oauth2state', $this->client->getState());
            //Redirect the user to the authorization URL.
            header('Location: ' . $authorizationUrl);
            exit;

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || ($_GET['state'] !== $session->get('oauth2state'))) {
            $session->remove('oauth2state');
            $this->getOutput()->addWikiText('Invalid state');
        }
    }

    /**
     * When coming back from KDE, take care of the login
     */
    private function _handleCallback() {
        global $wgKDELogin;

        $out = $this->getOutput();
        $request = $this->getRequest();

        try {
            // Try to get an access token using the authorization code grant.
            $accessToken = $this->client->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);

            $resourceOwner = $this->client->getResourceOwner($accessToken);

            // Let's get an oauth user
            $oauthId = $resourceOwner->getId();
            $oauthName = $resourceOwner->getName();
            $oauthNickname = $resourceOwner->getNickname();
            $oauthEmail = $resourceOwner->getEmail();
            $oauthSecondaryEmails = $resourceOwner->getSecondaryEmails();

            $externalId = $wgKDELogin['clientid'] . "." . $oauthId;

            $dbr = wfGetDB(DB_REPLICA);
            $extuser = KDEUser::newFromRemoteId($externalId, $oauthNickname, $oauthEmail, $accessToken->getToken(), wfGetDB(DB_REPLICA));

            if (0 === $extuser->getLocalId()) {
                $extuser = KDEUser::newFromEmail($externalId, $oauthNickname, $oauthEmail, $accessToken->getToken(), wfGetDB(DB_REPLICA), $oauthSecondaryEmails);
            }

            // KDE User already exists
            if ( 0 !== $extuser->getLocalId() ) {
                $user = User::newFromId($extuser->getLocalId());
                $extuser->updateInDatabase(wfGetDB(DB_MASTER));
                $user->invalidateCache();
                $this->getContext()->setUser($user);
                $user->setCookies($request, $this->isSecure() ? true: false , true);

                $out->addWikiMsg('kdelogin-successful');

            } else {
                // No KDE User yet
                $user = User::newFromName($oauthNickname);
                // The given local user already exists
                if (0 !== $user->getId()) {
                    $out->addWikiMsg('kdelogin-already-exists');
                } else {
                    $dbw = wfGetDB(DB_MASTER);
                    $user->setEmail($oauthEmail);
                    $user->addToDatabase();
                    $extuser->setLocalId($user->getId());
                    $extuser->addToDatabase($dbw);
                    $user->addNewUserLogEntry('create');
                    $this->getContext()->setUser($user);
                    $user->setCookies($request, $this->isSecure() ? true: false , true);
                    $out->redirect(SpecialPage::getTitleFor('Preferences')->getLinkUrl());
                }
            }

            if (false === $extuser || $extuser->getRemoteId() != 0) {
                throw new MWException('Unable to create new user account, please contact the Wiki administrator');
            }

        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

            // Failed to get the access token or user details.
            $this->getOutput()->addWikiText($e->getMessage());
        }
    }

    private function _default() {
        global $wgUser;

        $out = $this->getOutput();

        $out->setPageTitle( wfMessage('kdelogin-header-link-text')->text() );
        if ( !$wgUser->isLoggedIn() ) {
            $out->addWikiMsg( 'kdelogin-login-with-kde' );
        }
    }

    function isSecure() {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;
    }
}
