# Mediawiki KDE-Login Extension

This extension enables the use of the builtin KDE Oauth2 Server.
It uses the [OAuth2 Client Library] availabe via Composer.

## Installation

Clone or copy to your extensions directory, then
```bash
composer install
```

The necessary changes to LocalSettings.php are
```php
wfLoadExtension( 'KDELogin');
// Necessary Settings
$wgKDELogin['name'] = '<give the login a name>';
$wgKDELogin['clientid'] = '<your client id>';
$wgKDELogin['clientsecret'] = '<your client secret>';
$wgKDELogin['baseurl'] = '<the base url to your kde installation>';
// Optional
$wgKDELogin['login-text'] = '<the login text>';
$wgKDELogin['kdeonly'] = true; // Should core mw logins be disallowed?
```

Run "php maintenance/update.php" to add the necessary table.

[OAuth2 Client Library]: http://oauth2-client.thephpleague.com/
