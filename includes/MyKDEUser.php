<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

require __DIR__ . '/../vendor/autoload.php';

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class MyKdeUser implements ResourceOwnerInterface
{
    /**
     * Domain
     *
     * @var string
     */
    protected $domain;

    /**
     * @var array
     */
    protected $data;

    /**
     * MyKdeUser constructor.
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->data = $response;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getField('id');
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * @return mixed|null
     */
    public function getEmail(): ?string
    {
        return $this->getField('email');
    }

    /**
     * @return mixed|null
     */
    public function getName(): ?string
    {
        return $this->getField('name');
    }

    /**
     * @return array|null
     */
    public function getRole(): ?array
    {
        return $this->getField('roles');
    }

    /**
     * Return a field from the response
     * @param $key
     * @return mixed|null
     */
    private function getField($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    /**
     * Set resource owner domain
     *
     * @param  string $domain
     *
     * @return ResourceOwner
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * Get resource owner nickname
     *
     * @return string|null
     */
    public function getNickname()
    {
        return $this->getField['nickname'];
    }

    /**
     * Get resource owner url
     *
     * @return string|null
     */
    public function getUrl()
    {
        return trim($this->domain.'/'.$this->getNickname()) ?: null;
    }
}

