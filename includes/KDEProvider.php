<?php
/**
 * SPDX-FileCopyrightText: (c) 2016 Ingo Malchow <ingomalchow@gmail.com>
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-LicenseRef: AGPL-3.0-or-later
 */

namespace MediaWiki\Extensions\KDELogin;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/MyKDEUser.php';

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * This class extends the league/oauth2-client library
 *
 * It implements a Provider for KDE Installations
 */
class KDEProvider extends AbstractProvider
{
    use BearerAuthorizationTrait;

    /**
     * MyKDE API endpoint to retrieve logged in user information.
     *
     * @var string
     */
    const PATH_API_USER = '/api/me';

    /**
     * MyKDE OAuth server authorization endpoint.
     *
     * @var string
     */
    const PATH_API_AUTHORIZE = '/oauth/authorize';

    /**
     * MyKDE OAuth server token request endpoint.
     *
     * @var string
     */
    const PATH_API_TOKEN = '/oauth/token';

    /**
     * Domain
     *
     * @var string
     */
    public $domain = 'https://gallien.kde.org';

    public function __construct($url, array $options = [], array $collaborators = [])
    {
        parent::__construct($options, $collaborators);
        $this->domain = $url;
    }

    /**
     * Get authorization url to begin OAuth flow
     *
     * @return string
     */
    public function getBaseAuthorizationUrl()
    {
         return $this->domain . self::PATH_API_AUTHORIZE;
    }

    /**
     * Get access token url to retrieve token
     *
     * @param  array $params
     *
     * @return string
     */
    public function getBaseAccessTokenUrl(array $params)
    {
        return $this->domain . self::PATH_API_TOKEN;
    }

    /**
     * Get provider url to fetch user details
     *
     * @param  AccessToken $token
     *
     * @return string
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        return $this->domain . self::PATH_API_USER;
    }

    /**
     * Get the default scopes used by this provider.
     *
     * This should not be a complete list of all scopes, but the minimum
     * required for the provider user interface! KDE uses "whoami"
     * @TODO: make it configurable
     *
     * @return array
     */
    protected function getDefaultScopes()
    {
        return ['email'];
    }

    /**
     * Check a provider response for errors.
     *
     * @throws IdentityProviderException
     * @param  ResponseInterface $response
     * @param  string $data Parsed response data
     * @return void
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
    }

    /**
     * Generate a user object from a successful user details request.
     *
     * @param array $response
     * @param AccessToken $token
     * @return League\OAuth2\Client\Provider\ResourceOwnerInterface
     */
    protected function createResourceOwner(array $response, AccessToken $token): MyKdeUser
    {
        return new MyKdeUser($response);
    }
}


class MyKdeUser implements ResourceOwnerInterface
{
    /**
     * Domain
     *
     * @var string
     */
    protected $domain;

    /**
     * @var array
     */
    protected $data;

    /**
     * MyKdeUser constructor.
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->data = $response;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getField('id');
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * @return mixed|null
     */
    public function getEmail(): ?string
    {
        return $this->getField('email');
    }

    /**
     * @return mixed|null
     */
    public function getName(): ?string
    {
        return $this->getField('name');
    }

    /**
     * @return array|null
     */
    public function getRole(): ?array
    {
        return $this->getField('roles');
    }

    /**
     * @return array|null
     */
    public function getSecondaryEmails(): ?array
    {
        return $this->getField('secondaryEmails');
    }

    /**
     * Return a field from the response
     * @param $key
     * @return mixed|null
     */
    private function getField($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    /**
     * Set resource owner domain
     *
     * @param  string $domain
     *
     * @return ResourceOwner
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * Get resource owner nickname
     *
     * @return string|null
     */
    public function getNickname()
    {
        return $this->getField('nickname');
    }

    /**
     * Get resource owner url
     *
     * @return string|null
     */
    public function getUrl()
    {
        return trim($this->domain.'/'.$this->getNickname()) ?: null;
    }
}

