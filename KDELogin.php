<?php

if ( !defined( 'MEDIAWIKI' ) ) {
    die( 'This is a MediaWiki extension, and must be run from within MediaWiki.' );
}

if ( function_exists( 'wfLoadExtension' ) ) {
    wfLoadExtension( 'KDELogin' );
    // Keep i18n globals so mergeMessageFileList.php doesn't break
    $wgMessagesDirs['KDELogin'] = __DIR__ . '/i18n';
    $wgExtensionMessagesFiles['KDELoginAlias'] = __DIR__ . '/KDELogin.alias.php';

    /* wfWarn(
        'Deprecated PHP entry point used for Nuke extension. Please use wfLoadExtension instead, ' .
        'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
    ); */

    return true;
} else {
    die( 'This version of the KDELogin extension requires MediaWiki 1.25+' );
}
