<?php

class KDELoginHooks {

    public static function onPersonalUrls( array &$personal_urls, &$title, SkinTemplate $skin ) {
        global $wgKDELogin, $wgUser, $wgRequest;

        // Are we already logged in? if so, don't try a second time
        if( $wgUser->isLoggedIn() ) return true;

        $service_name = isset( $wgKDELogin['name'] ) && 0 < strlen( $wgKDELogin['name']) ? $wgKDELogin['name'] : 'OAuth2';

        if(isset( $wgKDELogin['login-text'] ) && 0 < strlen( $wgKDELogin['login-text'])) {
            $service_login_link_text = $wgKDELogin['login-text'];
        } else {
            $service_login_link_text = wfMessage($service_name)->text();
        }

        // Remove account creation and normal login boxes
        unset($personal_urls['createaccount']);
        unset($personal_urls['anonlogin']);
        if ($wgKDELogin['kdeonly'] === true) {
            unset($personal_urls['login']);
            unset($personal_urls['anonlogin']);
        }

        $personal_urls['anon_oauth_login'] = array(
            'text' => $service_login_link_text,
            //'class' => ,
            'active' => false,
        );

        $personal_urls['anon_oauth_login']['href'] = Skin::makeSpecialUrlSubpage( 'KDELogin', 'redirect' );

        if( isset( $personal_urls['anonlogin'] ) ) {
            $personal_urls['anonlogin']['href'] = Skin::makeSpecialUrl( 'Userlogin' );
        }
        return true;
    }

    public static function onUserLogout(&$user) {

        global $wgOut;

        return true;
    }

    public static function onLoadExtensionSchemaUpdates($updater = null) {
        switch ($updater->getDB()->getType() ) {
            case "mysql":
                return self::MySQLSchemaUpdates($updater);
            default:
                throw new MWException("KDELogin does not support {$updater->getDB()->getType()} yet.");
        }
    }

    /**
     * @param $updater MysqlUpdater
     * @return bool
     */
    public static function MySQLSchemaUpdates($updater = null) {
        $updater->addExtensionTable('external_user',
            dirname( __FILE__ ) . '/sql/kde-login.sql');

        return true;
    }
}
